---
title: "Participa"
date: 2017-09-17T19:33:48+02:00
draft: false
---


*¿Porqué participar?*

1. Para ayudarnos a difundir valores como la cooperación, ayuda mutua, solidaridad, bienestar y salud.
2. Para ayudarnos a erradicar el odio, la violencia y explotación.
3. Para promover la conservación de nuestra *Tierra* y eliminar el crecimiento progesivo de sustancias químicas contaminantes y materiales no biodegradables.
4. Para promover un uso ético de las tecnologías y beneficios científicos.
5. En definitiva, para construir una sociedad más sana para nosotr@s y nuestros hij@s.

*¿Cómo participar?*

1. Envíanos ideas sobre cómo mejorar, ¿qué servicios echas en falta? ¿tienes alguna idea y te gustaría ponerla en marcha?. Ponte en contacto con nosotr@s.

2. Date de alta como soci@ auto- ocupad@. Descubre sus ventajas aquí (todo: link a soporte de socios auto-ocupados).

3. Voluntariado: aceptamos personas que quieran colaborar con nosotr@s, si te apetece realizar algunas horas de voluntariado a la vez que nos vas conociendo mejor siempre sois bienvenid@s.

4. Donaciones monetarias: aceptamos moneda social, moneda elétronica ( Faircoin, Bitcoin) y Euros.

*todo: Poner botón donaciones

5. Testamento solidario: tú puedes seguir ayudando cuando ya no estés. Haz que tu solidaridad perdure.

Si quieres dejar un legado o una herencia a la Cooperativa elPoblet coméntalo a tu notario.
Nos lo puedes notificar llamándonos al teléfono -------------- o enviando un correo a -----------------------
