#!/bin/bash

<<LICENSE

Copyright (C) 2017  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENSE

cd "$( dirname "${BASH_SOURCE[0]}" )"

source funshell/prunedfind.sh
source funshell/md5check.sh
source funshell/githardupdate.sh

MD5SUM="$(prunedFind | md5check)"

gitHardUpdate &> lastlog

[ "${MD5SUMS}" == "$(prunedFind | md5check)" ] ||
{
	rm -rf /var/www/html
	/usr/local/bin/hugo --destination=/var/www/html/ &> lastlog
}
